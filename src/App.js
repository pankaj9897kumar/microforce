import './App.css';
import Header from './component/Header/Header';
import Banner from './component/Body/Banner';
import Footer from './component/Footer/footer'; 

function App() {
  //Try
  return (
    <div>
      <Header />
      <Banner />
      <Footer />
    </div>
  );
}

export default App;
