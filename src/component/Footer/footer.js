import React from "react";
import './footer.css';

function Footer() {

return(
    <div className="row color-change adjust-center pt-4">
        <div className="col-lg-3 col-sm-12 col-md-6">
            <h5>Microforce</h5>           
        </div>
        <div className="col-lg-3 col-sm-12 col-md-6">
            <ul className="list-type-none">
                <li>ORDER STATUS</li>
                <li>Track Order Status</li>
                <li>Installation Help</li>
                <li>Order Status Escalations</li>
            </ul>
            <ul className="list-type-none">
                <li>QUICK LINKS</li>
                <li>About Us</li>
                <li>Shipping Policy</li>
                <li>Return Policy</li>
                <li>Buy Back Offer</li>
                <li>Warranty Registration</li>
            </ul>
        </div>
        <div className="col-lg-3 col-sm-12 col-md-6">
            <ul className="list-type-none">
                <li className="mb-5">
                GET IN TOUCH <br/>
                Contact Us <br/>
                Address: <br/>
                Microforce Power Technologies Pvt. Ltd. <br/>
                CIN : U74899DL1988PTC032019
                </li>
                <li  className="mb-5">
                Corporate Office <br/> 
                Plot No. 150, Sector 44, <br/>
                Gurgaon, Haryana - 122003
                </li>
                <li  className="mb-5">
                Registered Office <br/>
                C-56, Mayapuri Industrial Area, Phase- II,<br/>
                Mayapuri, New Delhi 110064
                </li>
            </ul>
        </div>
        <div className="col-lg-3 col-sm-12 col-md-6">
            <p>LET US REACH YOU</p>
            <div className="input-group mb-3">
            <input type="text" className="form-control" placeholder="Enter your email" aria-label="Enter your email" aria-describedby="basic-addon2"/>
            <div className="input-group-append">
                <button className="btn btn-secondary" type="button">Subscriber</button>
            </div>
            </div>
        </div>
    </div>
)
}
export default Footer;