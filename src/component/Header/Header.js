import React, { useState } from "react";
import DropdownHover from '../../shared/DropdownHover'; 

function Header() {

    var headerNameList = [{
        navAnchor: 'HOME',
        child: false,
        childList: []
    }, {
        navAnchor: 'UPS/INVERTERS',
        child: true,
        childList: [{
            subcategoryImg: '',
            subTitle: 'a',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'b',
            link: ''
        }]
    }, {
        navAnchor: 'BATTERIES',
        child: true,
        childList: [{
            subcategoryImg: '',
            subTitle: 'a',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'b',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'c',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'd',
            link: ''
        }]
    }, {
        navAnchor: 'COMBO',
        child: true,
        childList: [{
            subcategoryImg: '',
            subTitle: 'a',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'b',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'c',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'd',
            link: ''
        }]
    }, {
        navAnchor: 'SOLAR',
        child: true,
        childList: [{
            subcategoryImg: '',
            subTitle: 'a',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'b',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'c',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'd',
            link: ''
        }]
    }, {
        navAnchor: 'TROLLIES',
        child: true,
        childList: [{
            subcategoryImg: '',
            subTitle: 'a',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'b',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'c',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'd',
            link: ''
        }]
    }, {
        navAnchor: 'FANS',
        child: true,
        childList: [{
            subcategoryImg: '',
            subTitle: 'a',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'b',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'c',
            link: ''
        }, {
            subcategoryImg: '',
            subTitle: 'd',
            link: ''
        }]
    }];



    return (
        <div className="d-flex justify-content-inline" style={{ backgroundColor:"linear-gradient(45deg, black, transparent)" }}>
            <div className="d-flex justify-content-center my-3 mx-3">
                <img height={"100px"} width={"150px"} src="/Logo/Microforce.png"></img>
            </div>
     <div className="d-flex justify-content-center  my-2">
        {
            headerNameList.map((e,i)=>{
            return    <div className="mx-5 my-5">
                <DropdownHover key={i} parent={e.navAnchor} child={e.childList}/>   
                </div>
            })
        }
    </div>
    </div>
    );
}

export default Header;