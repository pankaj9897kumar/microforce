import React from "react";
import { Carousel } from "react-bootstrap";
import './Banner.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons'

function Banner() {

    var categoryItem = [{
        img: '/Batteries/1.png',
        imgName: 'Inverters & Batteries Combo'
    }, {
        img: '/Batteries/2.png',
        imgName: 'Inverters & Batteries Combo'
    }, {
        img: '/Batteries/7.png',
        imgName: 'Inverters & Batteries Combo'
    }, {
        img: '/Batteries/8.png',
        imgName: 'Inverters & Batteries Combo'
    }];

    var topDeal = [{
        img:'/Batteries/3.png',
        title:'Battery 150 Ah - RC18000',
        price:'13,999',
        action: 'Buy Now'
    },{
        img:'/Batteries/4.png',
        title:'Battery 150 Ah - RC18000',
        price:'13,999',
        action: 'Buy Now'
    },{
        img:'/Batteries/5.png',
        title:'Battery 150 Ah - RC18000',
        price:'13,999',
        action: 'Buy Now'
    }
]

    return (
        <div>
            <div className="w-80 m-auto">
                <Carousel indicators={false} interval={3000}>
                    <Carousel.Item>
                        <div style={{ height: '300px' }} className="d-block w-70">
                            <img src="/Banner/Banner1.jpg"></img>
                        </div>
                    </Carousel.Item>
                    <Carousel.Item>
                        <div style={{ height: '300px' }} className="d-block w-70">
                        <img src="/Banner/Banner2.jpg"></img>
                        </div>
                    </Carousel.Item>
                    <Carousel.Item>
                        <div
                            style={{ height: '300px' }} className="d-block w-70">
                                <img src="/Banner/Banner3.jpg"></img>
                        </div>
                    </Carousel.Item>
                    <Carousel.Item>
                        <div
                            style={{ height: '300px' }} className="d-block w-70">
                                <img src="/Banner/Banner4.jpg"></img>
                        </div>
                    </Carousel.Item>
                </Carousel>
            </div>
            <div className="row m-2 bd-efefef mt-4">
                {
                    categoryItem.map((item, i) => {
                        return <div key={i} className="col-lg-3 col-sm-12 h-200 d-flex justify-content-center align-items-center">
                            <img height="200" width="200" src={item.img}></img>
                            <span>{item.imgName}</span>
                        </div>
                    })
                }
            </div>
            <div className="px-5 mb-1">
            <p>Top Deals</p>
            <div className="row justify-content-between mb-3">{
            topDeal && topDeal.map((item,i)=>{
            return <div key={i} className="col-lg-3 col-sm-12 col-md-6 box-shadow w-300">
                    <img height="200" width="200" src={item.img}></img>
                    <div>{item.title}</div>
                    <div>{item.price}</div>
                    <button className="btn btn-primary">{item.action}</button>
                </div>
            })   
            }
            <div className="col-lg-3 col-sm-12 col-md-6 box-shadow w-300
            d-flex justify-content-center align-items-center">                
                <FontAwesomeIcon icon={faPlus} />
                <span>   View More       </span>
            </div>
            </div>
            </div>
        </div>
    );
}
export default Banner;