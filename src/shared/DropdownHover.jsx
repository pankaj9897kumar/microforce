import React, { useState } from "react";
import {Dropdown} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import './DropdownHover.css';

function DropdownHover(props){
     const [menuOpen, toggleMenuOpen] = useState(false);
     console.log(props);
     const [style, setStyle] = useState({display: 'none'});

 return( 
  <Form>
   <div className="App ml-2" key={props.key}>
            <div onMouseUp={e => {
                     setStyle({display: 'flex'});
                 }}
                
            >
                <a>{props.parent}</a>
            </div>
        </div>
          <div className="zIndex display-child" style={style}  onMouseLeave={e => {
                     setStyle({display: 'none'})
                 }}>
        {
        props.child.map((e,index)=>{
          return     <a key={index}>{e.subTitle}</a>
        })    
      }    
       </div>

  </Form>
 );
}
export default DropdownHover;